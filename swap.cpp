//WAP to read 2 numbers and exchange their values

#include<iostream>
#include<conio.h>

using namespace std;

int main()
{
	int a,b,temp;
	
	cout<<"enter the first number to swap=";
	cin>>a;
	
	cout<<"enter the second number to swap=";
	cin>>b;
	
	temp=a;
	a=b;
	b=temp;
	
	cout<<"after swapping first no="<<a<<endl;
	cout<<"after swapping second no="<<b;
	
	getch();
	
	
}
