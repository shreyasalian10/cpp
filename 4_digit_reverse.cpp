//WAP to read 2 digit number and sum of its digit

#include<iostream>
#include<conio.h>

using namespace std;
int main()
{
	int n, fd, sd, sum;
	
	cout << "enter the number";
	cin >> n;
	
	cout << "You've entered: " << n;
	//45
	fd = n / 10; // 4
	sd = n % 10; // 5
	
	sum = fd + sd;
	
	cout << endl << "Addition of 2 digit number " << n << " is " << sum;
	
	
	getch();
}
